package oculus.sleep.eventlisteners;

import oculus.sleep.Sleep;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedLeaveEvent;

public class onBedLeave implements Listener {

    private final Sleep sleep = Sleep.getInstance();
    public boolean PercentageEnabled = sleep.getConfig().getBoolean("PercentageEnabled");
    public int Percentage = sleep.getConfig().getInt("Percentage.Percentage");
    public int Players = sleep.getConfig().getInt("Players.Players");
    public String PercentageMessageLeave = ChatColor.translateAlternateColorCodes('&', sleep.getConfig().getString("Percentage.MessageLeave"));
    public String PlayersMessageLeave = ChatColor.translateAlternateColorCodes('&', sleep.getConfig().getString("Players.MessageLeave"));
    public boolean Animation = sleep.getConfig().getBoolean("Animation");

    @EventHandler(ignoreCancelled = true)
    public void BedLeave(PlayerBedLeaveEvent e) {
        World world = Bukkit.getWorlds().get(0);

        if (Bukkit.getOnlinePlayers().size() == 1 && Animation) world.setTime(onBedEnter.time);

        if (sleep.playersSleeping.contains(e.getPlayer())) {
            sleep.playersSleeping.remove(e.getPlayer());
            int onlineplayers = Sleep.getOnlinePlayers();
            if (onlineplayers == 0) {
                onlineplayers = 1;
            }
            if (Sleep.isNight(world) && onlineplayers != 1) {

                int sleeping = sleep.playersSleeping.size();

                if (this.PercentageEnabled) {
                    float percentage = (float) sleeping / (float) onlineplayers * 100.0F;

                    Bukkit.broadcastMessage(
                            PercentageMessageLeave
                                    .replace("{Player}", e.getPlayer().getName())
                                    .replace("{Percentage}", String.valueOf((int)percentage))
                                    .replace("{NeededPercentage}", String.valueOf(this.Percentage))
                    );
                } else {
                    Bukkit.broadcastMessage(
                            PlayersMessageLeave
                                    .replace("{Player}", e.getPlayer().getName())
                                    .replace("{Players}", String.valueOf(sleeping))
                                    .replace("{NeededPlayers}", String.valueOf(this.Players))
                    );
                }
            }
        }

    }
}
