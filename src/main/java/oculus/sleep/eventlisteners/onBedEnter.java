package oculus.sleep.eventlisteners;

import oculus.sleep.Sleep;
import oculus.sleep.runnables.NightcycleAnimation;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.scheduler.BukkitTask;

public class onBedEnter implements Listener {

    private final Sleep sleep = Sleep.getInstance();
    public boolean PercentageEnabled = sleep.getConfig().getBoolean("PercentageEnabled");
    public int Percentage = sleep.getConfig().getInt("Percentage.Percentage");
    public int Players = sleep.getConfig().getInt("Players.Players");
    public String PercentageMessageEnter = ChatColor.translateAlternateColorCodes('&', sleep.getConfig().getString("Percentage.MessageEnter"));
    public String PlayersMessageEnter = ChatColor.translateAlternateColorCodes('&', sleep.getConfig().getString("Players.MessageEnter"));
    public boolean Animation = sleep.getConfig().getBoolean("Animation");
    public static long time;
    private BukkitTask task;

    @EventHandler(ignoreCancelled = true)
    public void onBedEnter(PlayerBedEnterEvent e) {
        int onlineplayers = Sleep.getOnlinePlayers();
        if (onlineplayers == 0) {
            onlineplayers = 1;
        }
        if (task != null && !task.isCancelled()) {
            if (onlineplayers == 1) {
                e.setCancelled(true);
            }
            return;
        }

        Player p = e.getPlayer();
        sleep.playersSleeping.add(p);

        boolean setday = false;
        int sleeping = sleep.playersSleeping.size();

        if (this.PercentageEnabled) {
            float percentage = (float) sleeping / (float) onlineplayers * 100.0F;

            Bukkit.broadcastMessage(
                    PercentageMessageEnter
                            .replace("{Player}", p.getName())
                            .replace("{Percentage}", String.valueOf((int) percentage))
                            .replace("{NeededPercentage}", String.valueOf(this.Percentage))
            );

            if (percentage >= (float) this.Percentage) {
                setday = true;
            }
        } else {
            Bukkit.broadcastMessage(
                    PlayersMessageEnter
                            .replace("{Player}", p.getName())
                            .replace("{Players}", String.valueOf(sleeping))
                            .replace("{NeededPlayers}", String.valueOf(this.Players))
            );
            if (onlineplayers < this.Players && onlineplayers == sleeping) {
                setday = true;
            } else if (sleeping >= this.Players) {
                setday = true;
            }
        }
        if (setday) {
            World world = Bukkit.getWorlds().get(0);

            world.setWeatherDuration(0);
            world.setThunderDuration(0);
            world.setThundering(false);
            world.setStorm(false);
            time = world.getFullTime();
            if (Animation) {
                task = new NightcycleAnimation(world).runTaskTimer(sleep, 0, 0);
            } else {
                world.setTime(0);
            }
            sleep.playersSleeping.clear();
        }
    }
}
